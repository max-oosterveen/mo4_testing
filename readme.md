# Python and MatLab

> https://nl.mathworks.com/products/matlab/matlab-and-python.html?requestedDomain=

## Quick start

```bash

pip install -r requirements.txt
pipenv shell

cd mo4_testing

python manage.py runserver
```

Server will run on localhost:8000, you can access the API by sending a request to http://localhost:8000/api/vessels/
