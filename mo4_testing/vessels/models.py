from django.db import models

class Vessel(models.Model):
  name = models.CharField(max_length=100)
  number_1 = models.IntegerField()
  number_2 = models.IntegerField()
  created_at = models.DateTimeField(auto_now_add=True)
