from rest_framework import viewsets, permissions
from .models import Vessel
from .serializers import VesselSerializer

class VesselViewSet(viewsets.ModelViewSet):
  queryset = Vessel.objects.all()
  permission_classes = [
    permissions.AllowAny
  ]

  serializer_class = VesselSerializer