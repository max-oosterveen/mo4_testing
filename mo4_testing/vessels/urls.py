# API
from rest_framework import routers
from .api import VesselViewSet

# API
router = routers.DefaultRouter()
router.register('api/vessels', VesselViewSet, 'vessels')

urlpatterns = router.urls